# Python_Articles


These are fun and play around with **Python**.
The **Python_Articles** is a set  of hands-on Python (Excel automation, File transefere, etc..)

**Let's Discover The Python capabilities together!**

# Sections

- [Excel automation 01](https://gitlab.com/Mohamed-Eleraki/powershell_articles/-/tree/main/Excel_automation_01#excel-automation-01)

- [File Transfer](https://gitlab.com/Mohamed-Eleraki/powershell_articles/-/tree/main/File_Transfer#file-transfer-)

- [File Transfer project](https://gitlab.com/Mohamed-Eleraki/powershell_articles/-/tree/main/File_Transfer_Project#-python-file-transfer-project-)

- [AWS Python automation_01 Fetch_VPC](https://gitlab.com/Mohamed-Eleraki/powershell_articles/-/tree/main/AWS%20Python%20automation#-aws-python-automation-)

- [AWS Python automation_02 Fetch_EC2](https://gitlab.com/Mohamed-Eleraki/powershell_articles/-/tree/main/AWS%20Python%20automation_02%20fetch_EC2#aws-python-automation-fetch_ec2-)
