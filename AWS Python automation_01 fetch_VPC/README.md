# 💫 AWS Python automation 💫 


AWS introduced **boto** library for Python, Which provides a powerful automation tool.
with **boto library**, you can do lots of things like **ec2, VPC creation**, etc; however, there's another powerful tools that provides **infrastructure as a service** like **Terraform**, which means Python Boto library very powerful for **query executer**, **fetching data, so forth**...
You can find the boto documentation [here](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)


## Discovery:

<details>
<summary><h3> How to..</h3></summary>

  - Open [boto package](https://pypi.org/project/boto3/)
  - Install the library by running the following:
   ``` pip install boto3 ```
  - make sure that the AWS configuration file is correct, and that you're able to communicate with AWS by aws cli.

<h3>Code explination</h3>

  - Open the **documentaion**, search for **'ec2'**, The 'ec2' Class have the most functions you going to use  >> [Here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html)
  
  - Search for **"describe_vpcs"** Will use this function to **collect** The **VPC** data >> [Here](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_vpcs.html)
  
  - At **ec_client** Variable, selected 'ec2' calss and specified the region
  - At **all_available_vpcs** Variable, Selected **'describe_vpcs'** function from **'ec2'** Class.
  - At **vpcs** Varibale, specified **Vpcs** list, So we can use it to loop on each dictionary in it.
  - check out **Response Syntax** in Docs **describe_vpcs**, The respose values it's a **list**, Thereofore to get it's values, we should to **loop** on the items.   
  - Now let's **iterate** and select the VpcId, CidrBlock, and State values.
  
</details>

```bash
import boto3

ec_client = boto3.client('ec2', region_name="us-east-1")
all_available_vpcs = ec_client.describe_vpcs()
vpcs = all_available_vpcs["Vpcs"]

# looping
for vpc in vpcs:
    vpc_id = vpc["VpcId"]
    cidr_block = vpc["CidrBlock"]
    state = vpc["State"]

    print(f"VPC ID: {vpc_id} with {cidr_block} state = {state}")

```

## References:
  - [Nana bootcamp](https://www.techworld-with-nana.com/devops-bootcamp)