# Moniroting & Repair WebSites






```bash
import os
import requests
import smtplib
import paramiko


def send_notification(email_msg):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: SiteDown\n{email_msg}"
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_PASSWORD, message)  # sender, receiver, and the message

try:
    response = requests.get('http://Example.com')
    if response.status_code == 200:
        print('Application is running successfully!')
    else:
        print('Application down, Fix it!')
        msg = f"Subject: Site Down\nApplication return {response.status_code}. Fix the issue!"
        send_notification(msg)

        # Restart the application, SSH connection
        ssh = paramiko.SSHClient()  # call the function
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())  # will connect for the 1st time will ask for set
        # messing host key policy this step to configure this

        # ssh connection with private key
        ssh.connect(hostname='139.162.130.236', username='root', key_filename='/home/mohamed/.ssh/id_rsa')

        # execute command
        stdin, stdout, stderr = ssh.exec_command('docker start CONTAINER_ID_HERE')  # to get the input value, the output, and the error msg
        print(stdin)
        print(stdout.readlines())
        ssh.close()
        print('Application restarted')

except Exception as ex:  # catch the err in ex
    print(f'connection error happened {ex}')  # print out the error
    msg = "Subject: SiteDown\napplication not accessible at all Fix the issue!"
    send_notification(msg)
```