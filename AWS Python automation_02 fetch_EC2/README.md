# AWS Python automation Fetch_EC2 ⚡

AWS introduced **boto** library for Python, Which provides a powerful automation tool.
with **boto library**, you can do lots of things like **ec2, VPC creation**, etc; however, there's another powerful tools that provides **infrastructure as a service** like **Terraform**, which means Python Boto library very powerful for **query executer**, **fetching data, so forth**...
You can find the boto documentation [here](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html)


## Brife

This example will play around with EC2, and will make a production example of how Python helps you to automate AWS resources.

**Steps of What We gonna Do:**
  - Fetch EC2 instances status
  - Schedule the script to run every 10 seconds

## Code Discovery *fetch ec2 data*

**#1**

```bash
import boto3

ec2_client = boto3.client('ec2')  # 1

reservations = ec2_client.describe_instances()  # 2
for reservation in reservations['Reservations']:  # 3
    instances = reservation['Instances']  # 4
    # print(instances)  # use print to test the result value
    for instance in instances:  # 5
        # print(instance['State'])  # print the status list
        # print(instance['State']['Name'])  # select name from status list
        print(f"Instance: {instance['InstanceId']} is {instance['State']['Name']}")  # 6
```
**Explanation**
  - Open boto documentaion/EC2 section [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html)
  - 1: Select **EC2** class *as apprears in docs*
  - 2: Select **describe_instances** function *discover response section to view what the function has and the result value*
  - 3: Select the **'Reservations'** list and iterate in it. 
  - 4: Select **'Instances'** Dictionary from **'Reservations'**
  - 5: Iterate on **'Instances'** values
  - 6: **Print** the result with selecting the the **value** of **instance** and **ingone the key.**




**#2** 


 **Another way** using **describe_instance_status** function, This function has more capabilities, view the docs for more [describe_instance_status Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_instance_status.html)


```bash
import boto3

ec2_client = boto3.client('ec2')  # 1
statuses = ec2_client.describe_instance_status( IncludeAllInstances=True )  # 2

for status in statuses['InstanceStatuses']:  # 3
  ins_status = status['InstanceStatus']['Status']  # 4
  sys_status = status['SystemStatus']['Status']  # 5
  state = status['InstanceState']['Name']  # 6
  print(f"Instance: {status['InstanceId']} status is {state} with instance status {ins_status} and system "
          f"status is {sys_status}")

 ```             
            
**Explanation**
  - Open boto documentaion/EC2 section [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html)
  - 1: Select **EC2** class *as apprears in docs*
  - 2: Select **describe_instance_status** function, With getting all instance statue *by defautl no enculde not running states* [describe_instance_status Docs](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2/client/describe_instance_status.html)
  - 3: Select the **'InstanceStatuses'** list and iterate in it. 
  - 4: Select **'InstanceStatus'** Dictionary and specify **Status** value in a ins_status variable
  - 5: Select **'SystemStatus'** Dictionary and specify **Status** value in a sys_status variable
  - 6: Select **'InstanceState'** Dictionary and specify **Name** value in a state variable



## Code Discovery *fetch ec2 data, and schedule script*

```bash
import boto3
import schedule

ec2_client = boto3.client('ec2')

def check_instance_status():
    statuses = ec2_client.describe_instance_status(IncludeAllInstances=True)

    for status in statuses['InstanceStatuses']:
        ins_status = status['InstanceStatus']['Status']
        sys_status = status['SystemStatus']['Status']
        state = status['InstanceState']['Name']
        print(f"Instance: {status['InstanceId']} status is {state} with instance status {ins_status} and system "
              f"status is {sys_status}")
        print("##############################\n")  # just a break

# schedule function
schedule.every(5).seconds.do(check_instance_status)  # 1

# while loop
while True:  # 2
    schedule.run_pending()

```

**Explination**
  - In this example, we just created a function that has the last code that we tried out.
  - Start the schedule every 10 sec, with selecting **check_instance_status** function. 
    Also, you can use the following
    ```bash
    
        schedule.every(5).minutes.do(check_instance_status)
        schedule.every().hours
        schedule.every().day.at("1:00") # 1 AM
        schedule.every().monday.at("12:00")

    ```
  - Start while loop with true value to make sure the script is working all the time


  ## References:
  - [Nana bootcamp](https://www.techworld-with-nana.com/devops-bootcamp) 