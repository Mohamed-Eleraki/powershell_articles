#  al sweigart Book, pages 232

import shutil, os, zipfile
from pathlib import Path
import send2trash

p = Path.home()  # Path.home() equal "WindowsPath('C:/Users/pythonuser')"

# Copy a file
# shutil.copy(p / 'test.txt', p / 'test')
# shutil.copy(p / 'test.txt', p/ 'test/renamedTest.txt')

# copy a directory with recurse
# shutil.copytree(p / 'test', p / 'testBackup')  # will create the testBackup directory for you,
# if the file exist will give an err

# Move files | if the file exist will be overwritten | in This case will not copy into testBackup instead
# will rename the file to testBackup, even if you put \\ after testBackup will be the same result
# even if you wrote it in this way shutil.move(p / 'test.txt', p / 'testBackup')
# shutil.move(p / 'test.txt', 'C:\\Users\\pythonuser\\testBackup')

# Move and rename | page 233
# shutil.move('C:\\bacon.txt', 'C:\\eggs\\new_bacon.txt')

# Delete
"""
• Calling os.unlink(path) will delete the file at path.
• Calling os.rmdir(path) will delete the folder at path. This folder must be empty of any files or folders.
• Calling shutil.rmtree(path) will remove the folder at path, and all files and folders it contains will also be deleted
"""

# for filename in Path.home().glob('*.rxt'):  # loop and delete items
# for filename in p.glob('*.rxt'):  # p.glob = Path.home as defined above
#     print(filename)
# os.unlink(filename)

# soft delete | send2trash
# pip install --user send2trash
# defined above

# testFile = open('/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/test.txt', 'a')  # Create the file
# testFile.write('This file for send2trash test package')
# testFile.close()
# send2trash.send2trash('/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/test.txt')

# Note that send2trash function can only send file to the recycle bin; it cannot pull file out of it.


# Walking a directory tree
# loop on files, folders TREE
# import os

for folderName, subfolders, filenames in os.walk(
        '/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/test'):
    print('The current folder is: ' + folderName)

    for subfolder in subfolders:
        print('subfolder of ' + folderName + ': ' + subfolder)

    for filename in filenames:
        print('file inside ' + folderName + ': ' + filename)
print('End os.walk function')
# Place the print function with your code action

# Python Conditions and If statements
#
# Python supports the usual logical conditions from mathematics:
#
#     Equals: a == b
#     Not Equals: a != b
#     Less than: a < b
#     Less than or equal to: a <= b
#     Greater than: a > b
#     Greater than or equal to: a >= b
# https://www.w3schools.com/python/gloss_python_if_statement.asp


# w3 school search result
# Import os Library
# import os
#
# # Travers all the branch of specified path with file descriptor
# for(root, dirs, files, rootfd) in os.fwalk('/var/'):
#     print("Directory path: %s"%root)
#     print("Directory Names: %s"%dirs)
#     print("Files Names: %s"%files)
#     print("File Discriptor: %s"%rootfd)


# compressing files with the zipfile Module
# import zipfile, os
# from pathlib import Path
#
# example_zip = zipfile.ZipFile('/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/testZipfile/test.zip')
# print(example_zip.namelist())  # print zipped file content
# test1_info = example_zip.getinfo('test1/test1.txt')  # get file information
# print(test1_info)
# print(test1_info.file_size)
# print(test1_info.compress_size)
# print(test1_info.compress_type)
# print(f'Compressed file is {round(test1_info.file_size / test1_info.compress_size, 2)}x Smaller!')  # this command
# # calculate how efficiently test.zip is compressed by dividing the original file size by compressed file size and
# # prints this information
#
# # Extract
# example_zip.extractall()  # Extract in a new directory created
# example_zip.extract('test1/test1.txt')  # Extract a specific file
# example_zip.extract('test1/test1.txt', '/home/another_directory')  # Extract a specific file to a new directory
#
# example_zip.close()
# # page 238

# Creating and adding to ZIP file
# import zipfile

new_zip = zipfile.ZipFile('/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/testZipfile/new.zip', 'w')
new_zip.write('/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/testZipfile/test1',
              compress_type=zipfile.ZIP_DEFLATED)
new_zip.write('/home/pythonuser/Documents/Python_Automation/projects/File_Transfer/testZipfile/test2/*',
              compress_type=zipfile.ZIP_DEFLATED)  # Note that this will just add directories without subdirectories
# of files
new_zip.close()
# to append to an existing zip file use a instead w

# python list files based on last access time
# https://thispointer.com/python-get-list-of-files-in-directory-sorted-by-date-and-time/
# https://stackoverflow.com/questions/168409/how-do-you-get-a-directory-listing-sorted-by-creation-date-in-python
