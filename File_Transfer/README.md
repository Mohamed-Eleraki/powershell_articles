# File Transfer 🤔 

**File transfer** is one of the most common cases you'll require support at many companies *(e.g. coping files, deleting old files, archiving files, backup files)* you can transform it into a **quick-working** file clerk who **never makes mistakes.**

**Any repetitive task must be automated, Why!?**🔭
  - Exploit the time to do something more useful.
  - Accelerate the process.
  - avoid human faults.

It will be **hasel tasks** if you are used to **manually** handling your tasks, on the other hand, you can **automate your tasks** and grab a coffee and enjoy monitoring.🌱

**Python** provides a wide range of libraries and functions to assist you to **automate The file transfer process** *(e.g. shutil, os, etc..)* **enough taking, and Let's discover:**⚡ 

## ⚡Example⚡  

**In this example** will build a tiny **script** that serve **file transfer process** by reading from an **Excel sheet**, This is an **official** way to build a script function, to **avoid script edit** in case any update happens, and enable it from the spreadsheet *(i.e. let's assume you want to transfer files from multiple paths to multiple paths, instead to make all conditions in the script make it as values in the spreadsheet and the script will read these values from it, in this scenario if you want to append some paths you will append in the spreadsheet instead of make a new step in your script, with this behavior you avoid editing the script, to ensure the script working on production as expected, Don't worry the next example explains what we talked about here)*

```bash
import pprint
import openpyxl
import shutil, os
from pathlib import Path
import send2trash

# Load the spreadsheet in a variable, and take a look at the spreadsheet to understand the next steps
spreadsheet_file = openpyxl.load_workbook("/home/pythonuser/Documents/spreadsheet.xlsx")
sheet = spreadsheet_file["Sheet1"]  # determine sheet1 to work with

# Looping on each line in the spreadsheet
for line in range(2,sheet.max_row + 1):  # Get data from a spreadsheet, 2 means start reading from row num 2 to ignore headers, + 1 is to get the last line by default won't catch it
    function_name = sheet.cell(line,1).value  # get the value of cell number one at the first column in a var named function_name
    source_path = sheet.cell(line, 2).value  # get the the value of cell number two
    destination_path = sheet.cell(line, 3).value  # get the value of cell number three

    print(f'Coping from the {source_path} to {destination_path}')
    shutil.copytree(source_path, destination_path)  # copy all to dest, don't create the dest directory will create
    # it for you

    print(f'Delete the source dir: {source_path}')
    send2trash.send2trash(source_path)  # delete the source path

```


**Also you can loop on a specific path useing the following:**

```bash
for folderName, subfolders, filenames in os.walk('/home/pythonuser/Documents/test'):
    print('The current folder is: ' + folderName)  # Replace the print function with the command executer that you want

    for subfolder in subfolders:
        print('subfolder of ' + folderName + ': ' + subfolder)  # replace the print function with the command executer that you want

    for filename in filenames:
        print('file inside ' + folderName + ': ' + filename)  # replace the print function with the command executer that you want
print('End os.walk function')
```



## References:
  - [Automate the Boring Stuff with Python Programming](https://www.amazon.com/Automate-Boring-Stuff-Python-2nd/dp/1593279922/ref=sr_1_1?keywords=Automate+the+Boring+Stuff+with+Python+Programming&link_code=qs&qid=1684181398&sourceid=Mozilla-search&sr=8-1)