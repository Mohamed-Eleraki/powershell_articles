import pprint
import openpyxl
import shutil, os
from pathlib import Path
import send2trash

# Load the spreadsheet in a variable, take a look on the spreadsheet to understand the next steps
spreadsheet_file = openpyxl.load_workbook(
    "/home/mohamed/Documents/Python_Automation/projects/File_Transfer/fileTransefer_Project/Articles/spreadsheet.xlsx")
sheet = spreadsheet_file["Sheet1"]  # determine sheet1 to work with

# Looping on each line in the spreadsheet
for line in range(2,
                  sheet.max_row + 1):  # Get data from spreadsheet, 2 means start reading from row num 2 to ignore headers, + 1 is to get the last line by default wont catch it
    function_name = sheet.cell(line,
                               1).value  # get value of cell number one at the first column in a var named function_name
    source_path = sheet.cell(line, 2).value  # get the value of cell number two
    destination_path = sheet.cell(line, 3).value  # get the value of cell number three

    print(f'Coping from the {source_path} to {destination_path}')
    shutil.copytree(source_path, destination_path)  # copy all to dest, don't create the dest directory will create
    # it for you

    print(f'Delete the source dir: {source_path}')
    send2trash.send2trash(source_path)  # delete the source path


