# Moniroting WebSites






```bash
import requests


try:  # using try and except for error handling
    response = requests.get('http://Example.com')
    if response.status_code == 200:
        print('Application is running successfully!')
    else:
        print('Application down, Fix it!')
        # in this step you can >> ssh to repair the server | send an email, etc...

except Exception as ex:  # catch the err in ex
    print(f'connection error happened {ex}')  # print out the error
    # Also here you can send an email for notify the server owner
```